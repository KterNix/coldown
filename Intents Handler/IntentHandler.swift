//
//  IntentHandler.swift
//  Intents Handler
//
//  Created by Nguyen The Quyen on 11/28/18.
//  Copyright © 2018 MobDesign. All rights reserved.
//

import Intents

class IntentHandler: INExtension,INStartWorkoutIntentHandling, INEndWorkoutIntentHandling {
    func handle(intent: INStartWorkoutIntent, completion: @escaping (INStartWorkoutIntentResponse) -> Void) {
        print("Start Workout Intent:", intent)
        
        let userActivity: NSUserActivity? = nil
        guard let spokenPhrase = intent.workoutName?.spokenPhrase else {
            completion(INStartWorkoutIntentResponse(code: .failureNoMatchingWorkout, userActivity: userActivity))
            return
        }
        
        print(spokenPhrase)
        
        completion(INStartWorkoutIntentResponse(code: .handleInApp, userActivity: userActivity))
    }
    
    func handle(intent: INEndWorkoutIntent, completion: @escaping (INEndWorkoutIntentResponse) -> Void) {
        
    }
    
    
    
}
