//
//  ViewController.swift
//  cameraButton
//
//  Created by Olivier Destrebecq on 3/15/16.
//  Copyright © 2016 MobDesign. All rights reserved.
//

import UIKit
import Intents
class ViewController: MasterViewController, PresenterOutPut, GetInfo {
    func didSetNew(value: String?) {
        goOnLabel.text = value
    }
    
//    var presenter: PresenterInPut?
    @IBOutlet weak var imageDemo: UIImageView!
    
    @IBOutlet weak var goOnLabel                  : UILabel!
    @IBOutlet weak var restLabel                  : UILabel!
    @IBOutlet var buttonsDuration                 : [UIButton]!
    @IBOutlet weak var pickerView                 : UIPickerView!
    @IBOutlet weak var bottomConstraintPickerView : NSLayoutConstraint!
    @IBOutlet weak var switchChangeTimeWork       : UISwitch!
    @IBOutlet weak var startOrPauseButton         : UIButton!
    
    var isSelected = false
    var token:NSKeyValueObservation?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupUI()
        setupData()
        setupForSiri()
        getText()
    }
    func setupUI() {
        switchChangeTimeWork.thumbTintColor  = .green
        switchChangeTimeWork.tintColor       = .white
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        goOnLabel.isHidden = true
        restLabel.isHidden = true

    }
    func setupData() {
        
        pickerView.dataSource     = self
        pickerView.delegate       = self
        switchChangeTimeWork.isOn = false
        presenter?.getTimeModel()
       
    }

    func getText() {
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
            let text = UIPasteboard.general.items //else {return}
            guard let dic = text.first else {return}
            guard let image = dic["public.jpeg"] as? UIImage else {return}
            self.imageDemo.image = image
        }
        token = UIPasteboard.general.observe(\.string, options: [.old,.new], changeHandler: { [weak self] (paster, change) in
            self?.goOnLabel.text = paster.string
        })
//        UIPasteboard.general.addObserver(self, forKeyPath: #keyPath(UIPasteboard.string), options: [.new,.old], context: &observerContext)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let newValue = change?[.newKey] as? NSObject,
            let oldValue = change?[.oldKey] as? NSObject,
            !newValue.isEqual(oldValue) {
            
        }
    }
    func setupForSiri() {
        INPreferences.requestSiriAuthorization { (status) in
        }
        INVocabulary.shared().setVocabularyStrings(["start workout", "end workout"], of: .workoutActivityName)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomConstraintPickerView.constant = -150
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
    }

    @IBAction func changeTimeWork(_ sender: UISwitch) {
        switch sender.isOn {
        case true:
            bottomConstraintPickerView.constant = 0
        default:
            bottomConstraintPickerView.constant = -150
            
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func startOrPause(_ sender: Any) {
        isSelected = !isSelected
        presenter?.coldown(begin: isSelected)
    }
    @IBAction func firstTime(_ sender: Any) {
        guard let time = self.getTimeForButton(position: 0) else { return }
        presenter?.selectedDurationWork(value: time)
    }
    
    @IBAction func secondTime(_ sender: Any) {
        guard let time = self.getTimeForButton(position: 1) else { return }
        presenter?.selectedDurationWork(value: time)

    }
    
    @IBAction func thirdTime(_ sender: Any) {
        guard let time = self.getTimeForButton(position: 2) else { return }
        presenter?.selectedDurationWork(value: time)

    }
    
    @IBAction func fouthTime(_ sender: Any) {
        guard let time = self.getTimeForButton(position: 3) else { return }
        presenter?.selectedDurationWork(value: time)

    }
    
    @IBAction func fifthTime(_ sender: Any) {
        guard let time = self.getTimeForButton(position: 4) else { return }
        presenter?.selectedDurationWork(value: time)

    }
    func getTimeForExactlyButton(timeDuration: Int?) -> Int? {
        guard let time = timeDuration else {return nil}
        let minutes = time/60
        switch minutes {
        case 10: return 0
        case 15: return 1
        case 20: return 2
        case 30: return 3
        case 60: return 4
        default: return nil
        }
    }
    func getTimeForButton(position: Int) -> Int? {
        switch position {
        case 0: return 10*60
        case 1: return 15*60
        case 2: return 20*60
        case 3: return 30*60
        case 4: return 60*60
        default: return nil
        }
    }
    //MARK:- Presenter OutPut
    func completed(text: String) {
        guard text != "Re" else {
            goOnLabel.text = "Go On"
            restLabel.text = "Rest"
            return
        }
        if text.contains("Completed") {
            startOrPauseButton.isSelected = !startOrPauseButton.isSelected
            startOrPause(startOrPauseButton)
        }
        self.goOnLabel.text = text
        self.restLabel.text = text
    }
    
    func hiddenGoOn(value: Bool) {
        self.goOnLabel.isHidden = value
        self.restLabel.isHidden = !value
    }
    
    func time(model: TimeModel) {
        guard let indexButton = getTimeForExactlyButton(timeDuration: model.timeDuration) else {return}
        
        let button                 = buttonsDuration[indexButton]
        button.layer.cornerRadius  = 5
        button.layer.borderColor   = UIColor.white.cgColor
        button.layer.borderWidth   = 1
        button.layer.masksToBounds = true
        let index                  = model.timeWork - 1
        
        buttonsDuration.forEach { (buttonList) in
            guard buttonList != button else {return}
            buttonList.layer.borderWidth = 0
        }
        
        pickerView.selectRow(index, inComponent: 0, animated: true)
    }
    deinit {
        print("==================")
        print(#file)
        print("Deinit ===========")
    }
    
}
extension ViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 60
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 20
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return view.frame.width
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.presenter?.selectedTimeWork(value: row + 1)
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let text = NSAttributedString(string: (row + 1).description + "s", attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor.white]))
        return text
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
