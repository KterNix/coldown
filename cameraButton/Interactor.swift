//
//  Interactor.swift
//  cameraButton
//
//  Created by Nguyen The Quyen on 11/20/18.
//  Copyright © 2018 MobDesign. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import AVFoundation
enum Sound:String{
    case Start = "start"
    case Paused = "paused"
    case Completed = "completed"
    case GetReady = "10second"
    case Song = "I"
}

class Interactor: InteractorInPut {
    
    var entity : TimeModel?
    
    var timerWork     :Timer?
    var timerRest     :Timer?
    var timerDuration :Timer?
    
    weak var outPut: InteractorOutPut?
    var player: AVAudioPlayer?
    
    var isPaused            = false
    var timeLessForRest     = 0
    var timeLessForWork     = 0
    var timeLessForDuration = 0
    var timeRest            = 30
    
    func startColdown() {
        if let timeInterval = player?.duration  {
            if timeInterval > 10 {
                DispatchQueue.global(qos: .default).async {
                    self.player?.play()
                }
            }
        }
        callTimeDuration()
        if isPaused {
            outPut?.completed(text: "Re")
            isPaused = false
            guard timeLessForWork == 0 else {
                callTimeWork()
                return}
            callTimeRest()
        }else {
            callTimeRest()
        }
    }
    
    func callTimeWork() {
        setTimeForWork()
        setTextForWork()
        timerWork = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.timeLessForWork -= 1
            
            if self.timeLessForWork == 10  {
                self.caution10SecondLeft()
            }
            
            guard self.timeLessForWork == 0 else {
                self.setTextForWork()
                return}
            
            timer.invalidate()
            
            self.setTextForWork()
            self.callTimeRest()
            self.pauseSound()

            
        })
    }
    func callTimeRest() {
        setTimeForRest()
        setTextForRest()
        outPut?.hiddenGoOn(value: true)
        timerRest = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.timeLessForRest -= 1
            
            if self.timeLessForRest == 10  {
                self.caution10SecondLeft()
            }
            
            guard self.timeLessForRest == 0 else {
                self.setTextForRest()
                return}
            
            timer.invalidate()
            self.setTextForRest()
            self.startSound()
            self.callTimeWork()
            self.timerRest = nil
            self.outPut?.hiddenGoOn(value: false)
            
        })
    }
    func callTimeDuration() {
        setTimeForDuration()
        timerDuration = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.timeLessForDuration -= 1
            guard self.timeLessForDuration == 0 else { return }
            
            self.pauseColdown()
            self.outPut?.completed(text: "Completed...")
            self.completedSound()
            self.timeLessForRest = 0
            self.timeLessForWork = 0
            self.isPaused = false
        })
    }
    private func setTextForRest() {
        self.outPut?.completed(text: "Rest...\(self.timeLessForRest)")
        
    }
    private func setTextForWork() {
        self.outPut?.completed(text: "Go On...\(self.timeLessForWork)")
        
    }
    private func setTimeForWork() {
        timeLessForWork = timeLessForWork == 0 ? (entity?.timeWork)! : timeLessForWork
    }
    private func setTimeForRest() {
        timeLessForRest = timeLessForRest == 0 ? timeRest : timeLessForRest

    }
    private func setTimeForDuration() {
        timeLessForDuration = timeLessForDuration == 0 ? (entity?.timeDuration)! : timeLessForDuration
    }
    private func startSound(){
//        AudioServicesPlayAlertSound(SystemSoundID(1113))
        play(sound: .Start)
    }
    
    private func pauseSound(){
//        AudioServicesPlayAlertSound(SystemSoundID(1114))
        play(sound: .Paused)
    }
    
    private func completedSound(){
        play(sound: .Completed)
    }
    private func caution10SecondLeft() {
        play(sound: .GetReady)
    }
    private func songSound() {
        play(sound: .Song)
    }
    private func play(sound:Sound, loops: Int = 1) {
        DispatchQueue.global(qos: .default).async {
            guard let url = Bundle.main.url(forResource: sound.rawValue, withExtension: sound == .Song ? "mp3" : "wav") else { return }
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: .default)
                try AVAudioSession.sharedInstance().setActive(true)
                self.player = try AVAudioPlayer(contentsOf : url, fileTypeHint: sound == .Song ? convertFromAVFileType(AVFileType.mp3) : convertFromAVFileType(AVFileType.wav))
                guard let player = self.player else { return }
                player.play()
                //Loops sound
                
                //            guard loops > 1 else { return }
                //            var count = 1
                //            Timer.scheduledTimer(withTimeInterval: player.duration + 1, repeats: true) { (timer) in
                //                guard loops == count else {
                //                    DispatchQueue.main.async {
                //                        player.play()
                //                        count += 1
                //                    }
                //                    return
                //                }
                //                timer.invalidate()
                //            }
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func pauseColdown() {
        player?.pause()
        isPaused = true
        timerWork?.invalidate()
        timerRest?.invalidate()
        timerDuration?.invalidate()
        timerRest     = nil
        timerDuration = nil
        timerWork     = nil
        outPut?.completed(text: "Paused...")
    }
    
    func selectedTimeWork(value :Int){
        entity?.timeWork = value
        saveData(timeModel: entity!)
    }
    
    func selectedDurationWork(value :Int) {
        entity?.timeDuration = value
        saveData(timeModel: entity!)
    }
    
    func getTimeModel() {
        guard let context = getContext() else { return }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Time")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            guard result.count > 0 else {
                saveNewTimeModel()
                return
            }
            for data in result as! [NSManagedObject] {
                let timeWork = data.value(forKey: "timeWork") as? Int
                let timeDuration = data.value(forKey: "timeDuration") as? Int
                entity = TimeModel(timeWork: timeWork, timeDuration: timeDuration)
                outPut?.time(model: entity!)
            }
            
        } catch {
            saveNewTimeModel()
            
        }
        
    }
    func saveNewTimeModel() {
        entity = TimeModel(timeWork: 30, timeDuration: 600)
        saveData(timeModel: entity!)
        outPut?.time(model: entity!)
    }
    func saveData(timeModel: TimeModel) {
        guard let context = getContext() else { return }
        let entity = NSEntityDescription.entity(forEntityName: "Time", in: context)
        let time = NSManagedObject(entity: entity!, insertInto: context)
        time.setValue(timeModel.timeDuration, forKey: "timeDuration")
        time.setValue(timeModel.timeWork, forKey: "timeWork")
        do {
            try context.save()
        }catch {
            print("Save Failed...")
            return
        }
        outPut?.time(model: self.entity!)

    }
    
    func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        let context = appDelegate.persistentContainer.viewContext
        return context
    }
    
    deinit {
        print("==================")
        print(#file)
        print("Deinit ===========")
    }
}
//extension Interactor :AVAudioPlayerDelegate {
//
//}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVFileType(_ input: AVFileType) -> String {
	return input.rawValue
}
