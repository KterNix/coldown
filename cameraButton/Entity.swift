//
//  Entity.swift
//  cameraButton
//
//  Created by Nguyen The Quyen on 11/20/18.
//  Copyright © 2018 MobDesign. All rights reserved.
//

import Foundation
class TimeModel {
    var timeWork : Int!
    var timeDuration : Int!
    init(timeWork:Int?, timeDuration:Int?) {
        self.timeWork = timeWork
        self.timeDuration = timeDuration
    }
}
