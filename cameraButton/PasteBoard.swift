//
//  PasteBoard.swift
//  cameraButton
//
//  Created by Nguyen The Quyen on 11/28/18.
//  Copyright © 2018 MobDesign. All rights reserved.
//

import UIKit
protocol GetInfo :class{
    func didSetNew(value:String?)
}
class PasteBoard: UIPasteboard  {
    
    weak var delegate:GetInfo?
   
    override var string: String? {
        didSet {
            self.delegate?.didSetNew(value: string)
        }
    }
}

