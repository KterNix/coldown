//
//  Protocol.swift
//  cameraButton
//
//  Created by Nguyen The Quyen on 11/20/18.
//  Copyright © 2018 MobDesign. All rights reserved.
//

import Foundation
import UIKit
// De cho Presenter goi
protocol InteractorInPut: class {
    var outPut:InteractorOutPut? {get set}
    
    func startColdown()
    func pauseColdown()
    func selectedTimeWork(value:Int)
    func selectedDurationWork(value:Int)
    func getTimeModel()
}
// De cho Presenter Implement, de cho Interactor goi
protocol InteractorOutPut: class {
    var interactor:InteractorInPut? {get set}

    func hiddenGoOn(value:Bool)
    func completed(text:String)
    func time(model:TimeModel)
}

// De cho VC goi
protocol PresenterInPut:class {
    var presenterOutPut:PresenterOutPut? {get set}
    func coldown(begin:Bool)
    func selectedTimeWork(value:Int)
    func selectedDurationWork(value:Int)
    func getTimeModel()
}

// Danh cho VC implement
protocol PresenterOutPut:class {
    
    var presenter:PresenterInPut? {get set}
    func completed(text:String)
    func hiddenGoOn(value:Bool)
    func time(model:TimeModel)
}
// De cho Presenter goi
protocol WarframeCall:class {
    var viewController: ViewController? {get set}
    
    
}
