//
//  Warframe.swift
//  cameraButton
//
//  Created by Nguyen The Quyen on 11/20/18.
//  Copyright © 2018 MobDesign. All rights reserved.
//

import Foundation
import UIKit
class Wireframe: WarframeCall {
    var interactor: Interactor?
    var presenter:Presenter?
    weak var viewController: ViewController?
    init(viewController:MasterViewController,completion:@escaping(MasterViewController) -> Void) {
        
        let interactor            = Interactor()
        let presenter             = Presenter()
        
        presenter.interactor      = interactor
        presenter.presenterOutPut = viewController as? PresenterOutPut
        presenter.wireframe       = self
        
        viewController.presenter            = presenter
        interactor.outPut         = presenter
        self.viewController     = viewController as? ViewController
        
        self.interactor = interactor
        self.presenter  = presenter
        completion(viewController)
    }
//    func createModule() -> ViewController? {
//        let view = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
//
//        let interactor            = Interactor()
//        let presenter             = Presenter()
//
//        presenter.interactor      = interactor
//        presenter.presenterOutPut = view
//        presenter.wireframe       = self
//
//        view.presenter            = presenter
//        interactor.outPut         = presenter
//        router.viewController     = view
//
//        self.interactor = interactor
//        self.presenter  = presenter
//
//        return view
//    }
    deinit {
        print("==================")
        print(#file)
        print("Deinit ===========")
    }
}
