//
//  Presenter.swift
//  cameraButton
//
//  Created by Nguyen The Quyen on 11/20/18.
//  Copyright © 2018 MobDesign. All rights reserved.
//

import Foundation
class Presenter:InteractorOutPut,PresenterInPut {
    weak var presenterOutPut: PresenterOutPut?
    weak var interactor: InteractorInPut?
    var wireframe: Wireframe?
    
    //MARK:- Interactor
    func coldown(begin: Bool) {
        begin ? self.interactor?.startColdown() : self.interactor?.pauseColdown()
    }
    
    func selectedTimeWork(value: Int) {
        self.interactor?.selectedTimeWork(value: value)
    }
    
    func selectedDurationWork(value: Int) {
        self.interactor?.selectedDurationWork(value: value)
    }
    
    func getTimeModel() {
        self.interactor?.getTimeModel()
    }
    
    //MARK:- Out Put
    func hiddenGoOn(value: Bool) {
        self.presenterOutPut?.hiddenGoOn(value: value)
    }
    
    func completed(text: String) {
        self.presenterOutPut?.completed(text: text)
    }
    
    func time(model: TimeModel) {
        self.presenterOutPut?.time(model: model)
    }
    
    deinit {
        print("==================")
        print(#file)
        print("Deinit ===========")
    }
}
